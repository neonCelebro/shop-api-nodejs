import React, {Component} from 'react';
import './App.css';
import {Route, Switch, BrowserRouter} from 'react-router-dom';

import Layout from "./components/Layout/Layout";
import Routes from "./Routes";

class App extends Component {

    render() {
        return (
            <BrowserRouter>
                <Layout>
                    <Switch>
                        <Routes/>
                    </Switch>
                </Layout>
            </BrowserRouter>

        );
    }
}

export default App;
