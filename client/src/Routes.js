import React from 'react';
import {connect} from 'react-redux';
import {Route, Switch, withRouter} from 'react-router-dom';

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import ProductForm from './containers/ProductForm/ProductForm';
import HomePage from './containers/HomePage/HomePage';


const Routes = () => {
    return (
        <Switch>
            <Route path='/' exact component={HomePage}/>
            <Route path='/addProduct' exact component={ProductForm}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
        </Switch>
    )
};

const mapStateToProps = state => {
    return {
        user: state.users.user
    }
};

export default withRouter(connect(mapStateToProps)(Routes));