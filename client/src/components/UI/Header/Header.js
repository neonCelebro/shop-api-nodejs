import React from 'react';
import {Nav, Navbar} from "react-bootstrap";
import {NavLink} from "react-router-dom";
import './Header.css';

const Header = (props) => {
    return (
        <div>
            <Navbar bg="dark" variant="dark">
                <Nav className="mr-auto">
                    <NavLink exact className='buttonNav' activeClassName='buttonNavActive' to="/">Home</NavLink>
                    <NavLink exact className='buttonNav' activeClassName='buttonNavActive' to="/addProduct">Add Product</NavLink>
                </Nav>
                {props.children}
            </Navbar>
        </div>
    )
};


export default Header;
