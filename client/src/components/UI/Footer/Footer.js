import React from 'react';
import {Navbar} from "react-bootstrap";

const Footer = () => (
    <Navbar fixed='bottom' bg="dark">
        <Navbar.Brand>Brand text</Navbar.Brand>
    </Navbar>
);

export default Footer;
