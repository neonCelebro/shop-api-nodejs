import React, {Fragment} from 'react';
import Header from "../UI/Header/Header";
import Toolbar from "../UI/Toolbar/Toolbar";

const Layout = props => {
    return (
        <Fragment>
            <Header>
                <Toolbar/>
            </Header>
            <main>
                {props.children}
            </main>
        </Fragment>
    )
};

export default Layout;
