import React, {Component} from 'react';
import {Form} from "react-bootstrap";
import {Button, Col} from "react-bootstrap";
import axios from '../../axios-shop';

class ProductFrom extends Component {

    state = {
        name: '',
        price: 0,
        description: '',
        category: '',
        image: '',
        categories: []
    };

    componentDidMount() {
        axios.get('categories').then(response => {
            this.setState({
                categories: response.data
            })
        })
    }

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        axios.post('products', formData).then(response => {
            console.log(response.data);
        });

        this.setState({
            name: '',
            description: '',
            image: '',
            price: 0,
            category: ''
        })
    };

    textHandler = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    fileHandler = event => {
        this.setState({[event.target.name]: event.target.files[0]});
    };

    selectChangeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    render() {
        return (
            <div className='justify-content-center'>
                <Form as={Col}>
                    <Form.Group controlId="productName">
                        <Form.Label>product name</Form.Label>
                        <Form.Control name='name' onChange={(e) => this.textHandler(e)} value={this.state.name}
                                      type="text" placeholder="Enter product name"/>
                    </Form.Group>
                    <Form.Group controlId="description">
                        <Form.Label>Description</Form.Label>
                        <Form.Control name='description' onChange={(e) => this.textHandler(e)}
                                      value={this.state.description} type="text" placeholder="description"/>
                    </Form.Group>
                    <Form.Group as={Col} controlId="price">
                        <Form.Label>Price</Form.Label>
                        <Form.Control name='price' onChange={(e) => this.textHandler(e)} value={this.state.price}
                                      type="number"/>
                    </Form.Group>

                    <Form.Group as={Col} controlId="productCategories">
                        Category
                        <Form.Control value="select"
                                      name='category'
                                      onChange={this.selectChangeHandler}
                                      as='select'
                                      required
                        >
                            <optgroup>
                                <option>Choose category</option>
                                {this.state.categories.map(category => {
                                    return (
                                        <option key={category._id} value={category._id}>
                                            {category.title}
                                        </option>
                                    )
                                })}
                            </optgroup>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group controlId="image">
                        <Form.Label>Add image</Form.Label>
                        <Form.Control name='image' onChange={(e) => this.fileHandler(e)} type="file"/>
                    </Form.Group>
                    <Button onClick={(e) => this.submitFormHandler(e)}>Send</Button>
                </Form>
            </div>
        )
    }
}

export default ProductFrom;
