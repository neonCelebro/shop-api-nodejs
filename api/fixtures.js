const mongoose = require('mongoose');
const config = require('./config');

const User  = require('./models/User');
const Product = require('./models/Product');
const Category = require('./models/Category');

mongoose.connect(config.db.url + '/' + config.db.name, {useNewUrlParser: true });

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('users');
        await db.dropCollection('products');
        await db.dropCollection('categories');
    } catch (e) {
        console.log('Collection were not present , skipping drop...');
    }

    console.log('collection is dropped');

    const [user, admin] = await User.create({
        username: 'user',
        password: '123',
        role: 'user'
    }, {
        username: 'admin',
        password: '123',
        role: 'admin'
    });
    console.log('use created');

    const [cpus, gpus] = await Category.create({
        title: 'CPUs',
        description: 'Here description for cpus'
    }, {
        title: 'GPUs',
        description: 'Here is description for GPUs'
    });
    console.log('categories created');

    const [gtx2080, gtx2080Ti, corei9, corei7] = await Product.create({
        name: 'gtx 2080',
        price: 1000,
        description: 'powerful gpu',
        image: "",
        category: gpus._id,
        userId: admin._id
    }, {
        name: 'gtx 2080Ti',
        price: 1000,
        description: 'top gpu',
        image: "",
        category: gpus._id,
        userId: admin._id
    }, {
        name: 'Core i 9',
        price: 1000,
        description: 'top cpu',
        image: "cpu.jpg",
        category: cpus._id,
        userId: admin._id
    }, {
        name: 'Core i 7',
        price: 1000,
        description: 'powerful cpu',
        image: "cpu.jpg",
        category: cpus._id,
        userId: admin._id
    });
    console.log('products were created');
    db.close();
});